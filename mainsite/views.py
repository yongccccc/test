from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from mainsite import models, forms
from django.contrib import auth


def index(request):
    if 'acc' in request.session:
        acc = request.session['acc']

    UQItems = models.UNIQLOItem.objects.all()
    UQItem_list = list()
    for _, UQItem in enumerate(UQItems):
        UQItem_list.append("{}".format(str(UQItem) + "<br>"))
    # return HttpResponse(UQItem_list)
    return render(request, 'index.html', locals())


def showgoods(request, id):
    if 'acc' in request.session:
        acc = request.session['acc']
    try:
        UQItem = models.UNIQLOItem.objects.get(UNIQLOID=id)
        if UQItem != None:
            return render(request, 'goodspage.html', locals())
    except:
        return redirect('/')


# member-seller-apply
def apply(request):
    if 'acc' in request.session:
        acc = request.session['acc']
    try:
        uniqlo_id = request.GET['uniqlo_id']
        itemexist = True
    except:
        uniqlo_id = 'None'
    return render(request, 'member/seller/apply.html', locals())

# member-login


def login(request):
    if request.method == 'POST':
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            acc = form.cleaned_data['acc']
            pwd = form.cleaned_data['pwd']

            if acc == '123' and pwd == '123123':
                isMember = True
            else:
                isMember = False
            message = '登入成功!'
        else:
            message = '登入失敗!'
    else:
        form = forms.LoginForm()

    try:
        if acc:
            request.session['acc'] = acc
    except:
        pass
    return render(request, 'member/login.html', locals())


def logout(request):
    if 'acc' in request.session:
        auth.logout(request)
    return HttpResponseRedirect('/')
    # Dispatch the signal before the user is logged out so the receivers have a
    # chance to find out *who* logged out.


# member


def register(request):
    if request.method == 'POST':
        form = forms.RegisterForm(request.POST)
        if form.is_valid():
            acc = form.cleaned_data['acc']
            pwd = form.cleaned_data['pwd']
            name = form.cleaned_data['name']
            tel = form.cleaned_data['tel']
            mail = form.cleaned_data['mail']

            if acc != None and pwd != None:
                member = models.Member(MemberID='流水號QQ', Acc=acc, Pwd=pwd, Name=name, Tel=tel,
                                       Mail=mail, LineRegistered=False, LineID='null')
                member.save()
                registered = True
            else:
                registered = False
            message = '註冊成功!'
        else:
            message = '註冊失敗!'
    else:
        form = forms.RegisterForm()

    return render(request, 'member/register.html', locals())


# member
def memberinfo(request):
    if 'acc' in request.session:
        acc = request.session['acc']
    return render(request, 'member/memberinfo.html', locals())

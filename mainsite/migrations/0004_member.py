# Generated by Django 2.1.5 on 2021-01-25 15:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainsite', '0003_salesrecord'),
    ]

    operations = [
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('MemberID', models.CharField(max_length=8)),
                ('Acc', models.CharField(max_length=30)),
                ('Pwd', models.CharField(max_length=15)),
                ('Name', models.CharField(max_length=30)),
                ('Tel', models.CharField(max_length=10)),
                ('Mail', models.CharField(max_length=30)),
                ('LineRegistered', models.BooleanField()),
                ('LineID', models.CharField(max_length=30)),
            ],
            options={
                'ordering': ['MemberID'],
            },
        ),
    ]

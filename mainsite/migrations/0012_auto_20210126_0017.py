# Generated by Django 2.1.5 on 2021-01-25 16:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mainsite', '0011_auto_20210126_0007'),
    ]

    operations = [
        migrations.CreateModel(
            name='UNIQLOItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('UNIQLOID', models.CharField(max_length=10)),
                ('UNIQLOTitle', models.CharField(max_length=20)),
                ('ClothesType', models.CharField(choices=[('outer-casual-outer', '外套類-休閒/機能/連帽外套'), ('outer-jacket', '外套類-風衣/大衣/西裝外套'), ('outer-ultralightdown', '外套類-特級極輕羽絨服'), ('outer-down', '外套類-羽絨外套'), ('outer-fleece', '外套類-刷毛系列'), ('bottoms-long-pants', '下身類-休閒長褲'), ('bottoms-jeans', '下身類-牛仔褲'), ('bottoms-long-pants', '下身類-休閒長褲'), ('bottoms-easy-and-gaucho', '下身類-九分褲'), ('bottoms-leggings', '下身類-緊身褲/內搭褲'), ('bottoms-widepants', '下身類-寬褲'), ('bottom-skirt', '下身類-裙子'), ('tops-short-sleeves-and-tank-top', '上衣類-短袖/背心'), ('tops-short-long-and-3-4sleeves-and-cardigan', '上衣類-長袖/七分袖'), ('tops-shirts-and-blouses', '上衣類-襯衫'), ('tops-sweat-collection', '上衣類-休閒/連帽上衣‧連帽外套'), ('tops-flannel', '上衣類-法蘭絨系列'), ('tops-knit', '上衣類-針織衫/毛衣/開襟外套'), ('tops-dresses', '上衣類-洋裝‧連身褲')], default='outer-casual-outer', max_length=50)),
                ('UNIQLOImage', models.ImageField(upload_to='')),
                ('Description', models.CharField(max_length=100)),
            ],
        ),
        migrations.DeleteModel(
            name='UNIQLOItems',
        ),
        migrations.AlterField(
            model_name='stock',
            name='UNIQLOID',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mainsite.UNIQLOItem'),
        ),
    ]
